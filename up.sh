#!/bin/bash
echo ""
echo "Odoo Community + Postgres"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $ODOO_CONTAINER_NETWORK
    docker network create $PG_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name ${SERVICE_NAME}_${PG_CONTAINER_VOLUME_DATA}
    docker volume create --name ${SERVICE_NAME}_${ODOO_CONTAINER_VOLUME_FILESTORE}
    docker volume create --name ${SERVICE_NAME}_${ODOO_CONTAINER_VOLUME_SESSIONS}
    mkdir -p $ODOO_CONTAINER_HOST_BIND/local-src
    
    if [ -f $ODOO_CONTAINER_HOST_BIND/startup.sh ]; then
        echo "templates already exists"
    else
        echo "create template files"

        cp templates/startup.sh.template $ODOO_CONTAINER_HOST_BIND/startup.sh
        chmod +x $ODOO_CONTAINER_HOST_BIND/startup.sh

        cp templates/preboot.sh.template $ODOO_CONTAINER_HOST_BIND/preboot.sh
        chmod +x $ODOO_CONTAINER_HOST_BIND/preboot.sh

        cp templates/addons_path.txt.template $ODOO_CONTAINER_HOST_BIND/local-src/addons_path.txt
        cp templates/marketplace_dependencies.txt.template $ODOO_CONTAINER_HOST_BIND/local-src/marketplace_dependencies.txt
        cp templates/oca_dependencies.txt.template $ODOO_CONTAINER_HOST_BIND/local-src/oca_dependencies.txt
        cp templates/odoo.conf.template $ODOO_CONTAINER_HOST_BIND/odoo.conf
        cp templates/requirements.txt.template $ODOO_CONTAINER_HOST_BIND/local-src/requirements.txt
    fi

    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1
    echo " "
    echo "Service should be available at this address: "${ODOO_CONTAINER_VHOST}${SERVICE_DOMAIN}
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env

    echo "" >> .env
    echo "TARGET_UID=$UID" >> .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

