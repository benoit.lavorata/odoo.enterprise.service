# community.odoo.service

Remember to add this config on nginx for longpolling

```config
## Start of configuration add by letsencrypt container
location ^~ /.well-known/acme-challenge/ {
    auth_basic off;
    allow all;
    root /usr/share/nginx/html;
    try_files $uri =404;
    break;
}
## End of configuration add by letsencrypt container

# Redirect longpoll requests to odoo longpolling port
location /longpolling {
    proxy_pass http://odoo:8072;
}
```